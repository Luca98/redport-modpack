set -eux

chmod +x packwiz

./packwiz modrinth export -o redport.mrpack

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
    --upload-file redport.mrpack \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/modpack/${CI_COMMIT_TAG}/redport.mrpack"
